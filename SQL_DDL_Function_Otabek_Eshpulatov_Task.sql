-- task 1

-- Create a function to determine the current quarter
CREATE OR REPLACE FUNCTION current_quarter()
    RETURNS integer AS
$$
BEGIN
    RETURN EXTRACT(QUARTER FROM CURRENT_DATE);
END;
$$ LANGUAGE plpgsql;

-- Create the view "sales_revenue_by_category_qtr"
CREATE OR REPLACE VIEW sales_revenue_by_category_qtr AS
SELECT fc.category_id,
       c.name                     AS category_name,
       COALESCE(SUM(p.amount), 0) AS total_sales_revenue
FROM public.film_category fc
         JOIN public.category c ON fc.category_id = c.category_id
         LEFT JOIN public.film f ON fc.film_id = f.film_id
         LEFT JOIN public.inventory i ON f.film_id = i.film_id
         LEFT JOIN public.rental r ON i.inventory_id = r.inventory_id
         LEFT JOIN public.payment p ON r.rental_id = p.rental_id
WHERE EXTRACT(QUARTER FROM r.rental_date) = current_quarter()
GROUP BY fc.category_id, c.name
HAVING COALESCE(SUM(p.amount), 0) > 0;



-- task 2
-- Create the function "get_sales_revenue_by_category_qtr"
CREATE OR REPLACE FUNCTION get_sales_revenue_by_category_qtr(p_current_quarter integer)
    RETURNS TABLE
            (
                category_id         smallint,
                category_name       TEXT,
                total_sales_revenue NUMERIC
            )
AS
$$
BEGIN
    RETURN QUERY
        SELECT fc.category_id,
               c.name                     AS category_name,
               COALESCE(SUM(p.amount), 0) AS total_sales_revenue
        FROM public.film_category fc
                 JOIN public.category c ON fc.category_id = c.category_id
                 LEFT JOIN public.film f ON fc.film_id = f.film_id
                 LEFT JOIN public.inventory i ON f.film_id = i.film_id
                 LEFT JOIN public.rental r ON i.inventory_id = r.inventory_id
                 LEFT JOIN public.payment p ON r.rental_id = p.rental_id
        WHERE EXTRACT(QUARTER FROM r.rental_date) = p_current_quarter
        GROUP BY fc.category_id, c.name
        HAVING COALESCE(SUM(p.amount), 0) > 0;
END;
$$ LANGUAGE plpgsql;



-- task 3
-- Create a stored procedure "new_movie"
CREATE OR REPLACE PROCEDURE new_movie(IN p_title TEXT)
    LANGUAGE plpgsql
AS
$$
DECLARE
    new_film_id INTEGER;
BEGIN
    -- Generate a new unique film ID
    SELECT COALESCE(MAX(film_id), 0) + 1 INTO new_film_id FROM public.film;

    -- Ensure the language "Klingon" exists in the "language" table
    IF NOT EXISTS (SELECT 1 FROM public.language WHERE name = 'Klingon') THEN
        RAISE warning 'Language "Klingon" does not exist in the language table.';
        insert into public.language (name) values ('Klingon');
    END IF;

    -- Insert a new movie into the film table
    INSERT INTO public.film (film_id,
                             title,
                             rental_rate,
                             rental_duration,
                             replacement_cost,
                             release_year,
                             language_id)
    VALUES (new_film_id,
            p_title,
            4.99,
            3,
            19.99,
            EXTRACT(YEAR FROM CURRENT_DATE),
            (SELECT language_id FROM public.language WHERE name = 'Klingon'));

END;
$$;

